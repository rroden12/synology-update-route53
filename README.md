# synology-update-route53

Synology friendly script to update a DNS record in Route 53. Modified version of the script by [Stephen Ostermiller](https://blog.ostermiller.org/updating-route53-dns-dynamically/)

Set variables at the top of the script


## Install
Assumes you already have the hosted zone and record ready in Route53

1. Install Python3 via Package center
2. Install pip
```
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3 ./get-pip.py
```
3. Add the pip install directory to your user's path 
```
echo "PATH=~/.local/bin:$PATH" > ~/.profile
```
4. Install awscli
```
python3 -m pip install awscli
```
6. Logout and back into activate changes
7. put updateRoute53.sh in your user's home directory
8. Change variables at the top of the script.
9. Create a task in Task Scheduler to run 
```
HOSTNAME="host.name.to.update" ZONEID="zoneIDFromRoute53" bash ~/updateRoute53.sh
``` 
On whatever schedule you prefer
